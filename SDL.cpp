# include "SDL.hpp"

SDLHandler::SDLHandler( int width, int height ) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		throw "Failed to init SDL!";
	}
	//SDL_WINDOWPOS_UNDEFINED
	if (!(this->window = SDL_CreateWindow("SDL Connectors Test", 50, 50,
	width, height, SDL_WINDOW_SHOWN))) {
		throw "Failed to create window!";
	}
	if (!(this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED))) {
		throw "Failed to create renderer!";
	}
	this->width = width;
	this->height = height;
}

SDLHandler::SDLHandler( SDLHandler &object ) {
	*this = object;
}

SDLHandler::~SDLHandler( void ) {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

SDLHandler	&SDLHandler::operator=( SDLHandler const &object ) {
	this->window = object.window;
	return *this;
}

void	SDLHandler::display( void ) {
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
}

void	SDLHandler::putpixel(unsigned int x, unsigned int y, unsigned int colour) {
	SDL_SetRenderDrawColor(renderer, colour >> 16, (colour >> 8) % 256, colour % 256, 255);
	SDL_RenderDrawPoint(renderer, x, y);
}

void	SDLHandler::fill_render(int width, int height, int colour) {
	for (int y = 0; y < height ; y++) {
		for (int x = 0; x < width ; x++) {
			putpixel(x, y, colour);
		}
	}
}

void	SDLHandler::clearpixels( void ) {
	SDL_RenderClear(renderer);
}

int  	SDLHandler::handleInput( void ) {
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT) {
			exit(0);
		}
		int keyCode = event.key.keysym.sym;
		if (keyCode == SDLK_ESCAPE) {
			exit(0);
		}
		return keyCode;
	}
	return 0;
}
