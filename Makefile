SDLINC	= `sdl2-config --cflags`
SDLLIB	= `sdl2-config --libs`

SDLNET =  -I /Library/Frameworks/SDL2_net.framework/Headers/ -L /Users/gsferopo/Downloads/SDL2_net/ -lSDL2_net 

INC		= -I network.hpp SDL.hpp
LIB		= -std=c++11 -lgnet
CC		= g++

SRC		= main.cpp \
			SDL.cpp \
			network.cpp

all:
	$(CC)  $(SRC) $(INC) $(LIB) $(SDLINC) $(SDLLIB) $(SDLNET)

lib:
	$(CC) -c network.cpp $(INC) $(SDLINC)
	ar rc libgnet.a network.o

re: lib all

