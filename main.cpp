# include "network.hpp"
# include "SDL.hpp"
# include <unistd.h>

# define SCREEN_WIDTH 500
# define SCREEN_HEIGHT 500

int main(int argc, char **argv) {
    SDLHandler sdl_(SCREEN_WIDTH, SCREEN_HEIGHT);
    UDPConnection udpConnection;
	
    std::string IP;
	int32_t localPort = 0;
	int32_t remotePort = 0;

    IP = "127.0.0.1";
    remotePort = argc > 1 ? 5555 : 4444;
    localPort = argc > 1 ?  4444 : 5555;
	udpConnection.Init( IP, remotePort, localPort );

    sdl_.fill_render(SCREEN_WIDTH, SCREEN_HEIGHT, 255 * 256);
    sdl_.fill_render(SCREEN_WIDTH, SCREEN_HEIGHT, 50);
    int input;
    bool    f = false;
    while ( !udpConnection.WasQuit() ) { 
        if (strcmp("100", udpConnection.recv_msg) == 0 && f == false) {
            puts("===================================");
            f = true;
            sdl_.fill_render(SCREEN_WIDTH, SCREEN_HEIGHT, 255 * 256);
            puts("===================================");
        }
        input = sdl_.handleInput();
        if (input) {
            udpConnection.msg = std::to_string(input);
        }
        udpConnection.Send();
        udpConnection.Recv();
        sdl_.display();
	}
}
