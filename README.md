# SDL 2 Network Lib

This project is a kind of wrapper I made to speed up development of any projects I make that use SDL 2
<br>
I took time to super simplify the network capabilty of SDL 2 to need as little amount of configuration and setting up as possible to get it to work in any project

SDL's network lib is non-blocking and compatible with all machines that are compatible will SDL (it can communicate with Windows, Mac and Linux machines without changing any code)

# How to use Network functions

first create a UDPConnection varible
calling the init method will initilize AND try to connect to a server if it exists, if a server does not exist it will become one, if a server does exist it will connect to the server
the server will manage communication to all clients connected to it
<br>
<br>
To send data change the 'dataToSend' varible and then call the send method
To recieve data use the recieve method then check the 'dataRecieved' varible, its NULL if nothing was sent to it
<br>
<br>
Theres some other functions to check but these are the ones I really use in my projects

You can check out the other network related methods <b>[here](network.hpp)</b>

# Important note
<b>non blocking</b>
I have also abstracted SDL 2 to make it easier to work with its display methods, [SDL Template](https://gitlab.com/Heagan/mylibs/tree/master/sdl_template)


# To install
Make sure to have SDL net installed, you can get it <b>[here](www.libsdl.org/projects/SDL_net)</b>
<br>Make sure to include the libraries header files and link it with `-lSDL2_net` 
<br><b>Theres an example Makefile of how I compiled projects with it


## Usage
```
#include "network.hpp"

UDPConnection udpConnection;
udpConnection.Init( IP, remotePort, localPort );

while ( !udpConnection.WasQuit() ) {
    udpConnection.msg = (void *)"Send Stuff";
    udpConnection.Send();
    udpConnection.Recv();
    void *stuff = udpConnection.recv_msg;
}
```










