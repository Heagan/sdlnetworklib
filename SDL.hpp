#ifndef SDL_H
# define SDL_H

# include <SDL.h>
// # include "net.hpp"

class SDLHandler {

public:
	SDLHandler( int width, int height );
	SDLHandler( SDLHandler &object );
	~SDLHandler( void );
	SDLHandler &operator=( SDLHandler const &object );
	int		handleInput();
	void	display();
    void	putpixel(unsigned int x, unsigned int y, unsigned int colour) ;
    void	clearpixels() ;
	void	fill_render(int width, int height, int colour);
	int		width;
	int		height;

	SDL_Window		*window;
	SDL_Renderer	*renderer;
	SDL_Event		event;

private:

};

#endif
